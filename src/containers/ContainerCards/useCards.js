// packages
import { useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
// actions
import { requestCards } from '../../actions/requestCards'

const useCards = () => {
  const dispatch = useDispatch()
  const cards = useSelector(state => state.cards.fetchedCards)
  const loading = useSelector(state => state.app.loading)

  const updateCards = useCallback(
    () => dispatch(requestCards())
    ,[dispatch])

  useEffect(() => {
    updateCards()
  }, [updateCards])

  return {
    cards,
    loading
  }
}

export default useCards
