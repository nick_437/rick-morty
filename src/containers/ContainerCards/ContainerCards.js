// package
import React from 'react'
import _ from 'lodash'
// components
import Card from '../../components/Card/Card'
import Loader from '../../components/Loader/Loader'
// hooks
import useCards from './useCards'

const ContainerCards = () => {
  const { cards, loading } = useCards()

  if (loading) {
   return (
     <Loader />
   )
  }

  return (
    <div className='row row-cols-1 row-cols-md-2'>
      {cards?.results && (
        _.map(cards?.results, (item, key) => (
          <Card
            key={key}
            {...item}
          />
        ))
      )}
    </div>
  )
}

export default ContainerCards