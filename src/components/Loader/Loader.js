// package
import React from 'react'

const Loader = () => {
  return (
    <div className='w-100 pt-5 text-center'>
      <div className='spinner-border text-dark' role='status'>
        <span className='sr-only'>Loading...</span>
      </div>
    </div>
  )
}

export default Loader