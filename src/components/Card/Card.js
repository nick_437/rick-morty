// package
import React from 'react'
import { Link } from 'react-router-dom'

const Card = ({ name, image, origin, location, status, species }) => {
  return (
    <div className='col mb-4'>
      <div className='row'>
        <div className='col'>
          {image && (
            <img
              src={image}
              className='card-img'
              alt={image}
            />
          )}
        </div>
        <div className='col'>
          <div className='card-body'>
            {name && (
              <h5 className='card-title'>{name}</h5>
            )}
            <p className='mb-3'>
              <span
                style={{
                  display: 'inline-block',
                  width: '.6rem',
                  height: '.6rem',
                  marginRight: '.6rem',
                  borderRadius: '50%',
                  backgroundColor:
                    status === 'Alive'
                      ? 'green'
                      : status === 'Dead'
                        ? 'red'
                        : 'grey'
                }}
              />
              {`${status} - ${species}`}
            </p>
            {origin?.name && (
              <div>
                <p className='mb-1'>Last known location:</p>
                <Link to={origin?.url}>{origin?.name}</Link>
              </div>

            )}
            {location?.name && (
              <div className='mb-3'>
                <p className='mb-1'>First seen in:</p>
                <Link to={location?.url}>{location?.name}</Link>
              </div>

            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card