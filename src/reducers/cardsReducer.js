// package
import cleanSet from 'clean-set'
// constants
import { FETCH_CARDS } from '../constants/REDUCERS'

const initialState = {
  fetchedCards: []
}

export const cardsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CARDS:
      return cleanSet(state, 'fetchedCards', action.payload)
    default:
      return state
  }
}