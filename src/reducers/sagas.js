//package
import { takeEvery, put, call } from 'redux-saga/effects'
// constants
import { FETCH_CARDS, REQUEST_CARDS } from '../constants/REDUCERS'
// actions
import { showLoader } from '../actions/showLoader'
import { hideLoader } from '../actions/hideLoader'

export const delay = (ms) => new Promise(res => setTimeout(res, ms))

export function* sagaWatcher () {
  yield takeEvery(REQUEST_CARDS, sagaWorker)
}

function* sagaWorker () {
  yield put(showLoader())
  const payload = yield call(fetchCards)
  yield call(delay, 1000)
  yield put({ type: FETCH_CARDS, payload })
  yield put(hideLoader())
}

async function fetchCards () {
  const response = await fetch('https://rickandmortyapi.com/api/character/')
  return await response.json()
}