// package
import { combineReducers } from 'redux'
// reducers
import { cardsReducer } from './cardsReducer'
import { appReducer } from './appReducer'

export const rootReducer = combineReducers({
  cards: cardsReducer,
  app: appReducer
})