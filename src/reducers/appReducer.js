// package
import cleanSet from 'clean-set'
// constants
import { HIDE_LOADER, SHOW_LOADER } from '../constants/REDUCERS'

const initialState = {
  loading: false
}

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return cleanSet(state, 'loading', true)
    case HIDE_LOADER:
      return cleanSet(state, 'loading', false)
    default:
      return state
  }
}