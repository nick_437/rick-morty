// constants
import { SHOW_LOADER } from '../constants/REDUCERS'

export function showLoader () {
  return {
    type: SHOW_LOADER
  }
}