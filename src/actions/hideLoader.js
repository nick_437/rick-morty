// constants
import { HIDE_LOADER } from '../constants/REDUCERS'

export function hideLoader () {
  return {
    type: HIDE_LOADER
  }
}