// constants
import { REQUEST_CARDS } from '../constants/REDUCERS'

export function requestCards () {
  return {
    type: REQUEST_CARDS
  }
}