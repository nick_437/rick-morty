// package
import React from 'react'
import cn from 'classnames'
// containers
import ContainersCards from '../../containers/ContainerCards/ContainerCards'
// style
import css from './PageHome.module.scss'

const PageHome = () => {
  return (
    <div className={cn(css.wrapper, 'wrapper')}>
      <div className='container pt-5'>
        <h1 className='text-center'>The Rick and Morty API</h1>
        <div className='row pt-5'>
          <ContainersCards />
        </div>
      </div>
    </div>
  )
}

export default PageHome