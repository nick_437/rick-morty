// package
import React from 'react'
import { Route, Switch } from 'react-router-dom'
// constants
import {
  PAGE_HOME
} from '../constants/ROUTES'

import PageHome from './PageHome/PageHome'


function Routes () {
  return (
    <Switch>
      <Route exact path={PAGE_HOME} component={PageHome} /> {/* Главная */}
    </Switch>
  )
}

export default Routes